import Vue from 'vue';
import App from './App.vue';

// import { str1, str2 } from './first';
// import sayMessage from './second';

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount('#app');

// sayMessage(str1);
// sayMessage(str2);
