export default [
  {
    title: 'Блендер REDMOND RHB-2949',
    price: 3095,
    image: '/img/blender.jpg',
    colors: [
      '#73B6EA',
      '#8BE000',
      '#222',
    ],
  },
  {
    title: 'Фен для волос Dyson Supersonic',
    price: 31990,
    image: '/img/dyson.jpg',
  },
  {
    title: 'Пылесос BBK',
    price: 4990,
    image: '/img/hoover.jpg',
    colors: [
      '#73B6EA',
      '#8BE000',
      '#222',
    ],
  },
  {
    title: 'Смартфон Apple iPhone Xs Max 256GB',
    price: 81090,
    image: '/img/apple.jpg',
  },
  {
    title: 'AirPods ES36',
    price: 7899,
    image: '/img/airpods.jpg',
    colors: [
      '#000000',
      '#F0FFF0',
    ],
  },
  {
    title: 'Зубная щетка электрическая Oral-b',
    price: 6690,
    image: '/img/oral_b.jpg',
    colors: [
      '#73B6EA',
      '#8BE000',
      '#222',
    ],
  },
  {
    title: 'Беспроводные наушники Everest',
    price: 2690,
    image: '/img/headphones.jpeg',
  },
  {
    title: 'Мультиварка Tefal F46',
    price: 5599,
    image: '/img/multi.jpg',
  },
  {
    title: 'Самокат Электрический Xiaomi',
    price: 3690,
    image: '/img/xiaomi.jpg',
  },
  {
    title: 'Велотренажер AMMITY DB 40',
    price: 27890,
    image: '/img/velo.jpg',
    colors: [
      '#73B6EA',
      '#8BE000',
      '#222',
    ],
  },
  {
    title: 'Samsung Galaxy Note10',
    price: 38999,
    image: '/img/Note10.jpg',
    colors: [
      '#8B0000',
      '#DCDCDC',
      '#000000',
    ],
  },
  {
    title: 'Смарт-часы Samsung Galaxy Watch Active 2',
    price: 18390,
    image: '/img/watch.jpeg',
    colors: [
      '#8B0000',
      '#DCDCDC',
      '#000000',
    ],
  },
  {
    title: 'ЖК Телевизор Philips',
    price: 49990,
    image: '/img/tv.jpeg',
  },
  {
    title: 'Минихолодильник HT-50A',
    price: 14200,
    image: '/img/refr.jpg',
  },
  {
    title: 'Гриль-барбекю Green Glade',
    price: 1990,
    image: '/img/gril.jpg',
  },
];
